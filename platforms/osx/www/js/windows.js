// Global variables
var dail_up_connection = false;
var sound_driver_loaded = "";
var sound_driver_selected = false;
var ransomware_code = false;
var interrupt = "";
var port = "";
var one = "3";
var two = "240";
var three = "3";
var four = "8";
var five = "5";

var topOffset = 0;
var leftOffset = 0;
var today = new Date();

function onLoad() {
  createNewWindow('Program_Manager', 'body');
  createNewWindow('Main', 'Program_Manager');
  // createNewWindow('Dail_up_networking', 'body');
}


// Check wich window is clicked
$(document.body).click(function(evt) {
  var clicked = evt.target;
  var currentID = clicked.id || "No ID!";
  console.log(` Deze = ${currentID.toString().split("_content")[0].split("_windowTitle")[0]}`);
  if (currentID.toString().split("_content")[0].split("_windowTitle")[0] !== "No ID!") {
    console.log(getObjects(this_json, "name", currentID.toString().split("_content")[0].split("_windowTitle")[0]).length)
    if (getObjects(this_json, "name", currentID.toString().split("_content")[0].split("_windowTitle")[0]).length > 0) {
      if ($(`#${currentID.toString().split("_content")[0].split("_windowTitle")[0]}`).hasClass('windowActive')) {
        console.log('already active');
      } else {
        setWindowActive(`#${currentID.toString().split("_content")[0].split("_windowTitle")[0]}`);
      }
    }
  }
})

function createNewWindow(name, parent) {
  if ($(`#${name}`).length) {
    setWindowActive(`#${name.id}`);
  } else {
    console.log(name)
    name = new Window(name, parent);
    $(parent === 'body' ? parent : `#${parent}_content`)
      .append($(name.Main()).css('position', 'absolute')
        .draggable(name.Draggable())
        .append($(name.TitleBar())
          .append($(name.MenuButton()).on('dblclick', function() {
            closeWindow(`#${name.id}`);
          }))
          .append(name.Title(name))
          .css('color', 'white')
          .append(name.MinButton())
          .append(name.MaxButton())
        )
        .append($(name.Content())
          .append($(name.IconGrid())
            .append(name.Icons())
          )
        )
        .resizable(name.Resizable())
        .on('click', function() {
          // setWindowActive(`#${name.id}`, parent);
        })
      );
    name.ConfigureIcons();
    checkWindowResizable(`#${name.id}`);
    checkForSpecialWindows(name.id);
    setWindowActive(`#${name.id}`);
  }
  
}

/////////////////////////////////////////////////////////////
// Window functions
//
function closeWindow(window) {
  if ($(window).attr('id') !== "Program_Manager") {
    $(`${window}_icon`).show();
    if ($(window).hasClass('windowFull')) {
      $('#Program_Manager_windowTitle').text('Program Manager');
    }
    $(window).remove();
  }
}

function minimizeWindow(window) {
  $(`${window}`).hide();
  if(!getObjects(this_json, '', window.replace('#', ''))[0]['application']) { 
    $(`${window}_icon`).show();
    if ($(`${window.replace('#', '')}_icon`).length > 0) {
      $(`${window.replace('#', '')}_icon`).show();
    }
  } else {
    let this_icon = getObjects(this_json, '', window.replace('#', ''))[0]['icon'];
    $("#iconGridDesktop").append($(`<div class="icon" id="${window.replace('#', '')}_desktopIcon"><img src="${this_icon}" width="56" height="47"><p>${window.replace('_', ' ').replace('#', '')}</div>`).css('bottom', '0')
      .draggable()
      .on('dblclick', function() {
        $(this).hide();
        $(window).show();
      })
      .on('click', function() {
        setIconActive(`${window.replace('#', '')}_desktopIcon`);
      })
    );
  }
}


function maximizeWindow(window) {
  // Store window dimensions
  let height = $(window).css('height').toString().replace('px', '');
  let width = $(window).css('width').toString().replace('px', '');
  let left = $(window).css('left').toString().replace('px', '');
  let top = $(window).css('top').toString().replace('px', '');

  if (!getObjects(this_json, '', window.replace('#', ''))[0]['application']) {
    $('#Program_Manager_windowTitle').text(`Program Manager - [${$(window).attr('id')}]`);
    $(window + '_buttonMin').hide();
    $(`${window}_windowTitle`).switchClass("windowTitleActive", "windowTitleFull", 10).switchClass("windowTitle", "windowTitleFull", 10).css('width', 'calc(100% - 18px)')
    setWindowActive('Program_Manager');
  }
  $(window)
    .css('padding', '0px')
    .css('border', '0px solid')
    .css('width', '100%')
    .css('height', '100%')
    .css('left', '0px')
    .css('top', '0px')
    .css('position', 'relative');
  $(window + '_buttonFull').replaceWith(`<a href="#" class="buttonResize" id="${window.replace('#', '')}_buttonResize" onClick="resizeWindow('${window}', ${width}, ${height}, ${left}, ${top})"></a>`)
  $(window).switchClass("window", "windowFull", 10);
  $(window).resizable("disable");
}

function resizeWindow(window, width, height, left, top) {
  if (window !== "#Program_Manager") {
    $('#Program_Manager_windowTitle').text('Program Manager');
  }
  $(window)
    .css('width', width)
    .css('height', height)
    .css('left', left)
    .css('top', top)
    .css('padding', '2px')
    .css('border', '1px solid')
    .css('position', 'absolute');
  $(window + '_buttonResize').replaceWith(`<a href="#" class="buttonMax" id="${window.replace('#', '')}_buttonFull" onClick="maximizeWindow('${window}')"></a>`);
  $(window).switchClass("windowFull", "window", 10);
  $(`${window}_windowTitle`).switchClass("windowTitleFull", "windowTitleActive", 10);
  $(window).resizable("enable");
  $(window + '_buttonMin').show();
}

function setWindowActive(window) {
  console.log(`Set window active = ${window}`);
  // Set all windows and titles not active
  $('.windowActive').switchClass("windowActive", "window", 1);
  $('.windowTitleActive').switchClass("windowTitleActive", "windowTitle", 1);
  // Set the rght window active
  if (window === "#Unlock_Screen") {
    $(`${window}_windowTitle`).switchClass("windowTitle", "windowTitleActive", 1);
  } else {
    $(`${window}_windowTitle`).switchClass("windowTitle", "windowTitleActive", 1);
    $(`${window}`).switchClass("window", "windowActive", 1);
  }
}

function setIconActive(icon) {
  $('.iconActive').switchClass('iconActive', 'icon', 10);
  $(`#${icon}`).switchClass('icon', 'iconActive', 10);
}

function checkWindowResizable(window) {
  if (!getObjects(this_json, '', window.replace('#', ''))[0]['resizable']) {
    $(window).resizable("disable");
  }
}

function checkForSpecialWindows(window) {
  if (window === "Dail_up_networking") {
    if (dail_up_connection) {
      closeWindow(`#${window}`);
      setWindowActive(`#Connection_Timer`);
    } else {
      let username = "bgates@dsu.nl";
      let password = "Yes!56k6";
      let tel_num = "0413"
      let tel_num2 = "758336"
      $(`#${window}_content`).append($(`<audio id="${window.split('_')[0]}">`).append(`<source src="./assets/dialup.mp3" type="audio/mpeg">`)).append(`</audio>`).append('<div class="thisIframeContent" style="padding:20px" align="left">\
      Connect to DSU\
      <p>\
      <table>\
        <tr>\
          <td>Username:</td>\
          <td><input type="text" id="Username" size="24"  ></td>\
        </tr>\
        <tr>\
          <td>Password:</td>\
          <td><input type="password" id="Password" size="24"></td>\
        </tr>\
        <tr>\
          <td><br></td>\
          <td><br></td>\
        <tr>\
          <td>Phone number:</td>\
          <td><input type="text" id="PhoneNumber" size="4"> - <input type="text" id="PhoneNumber2" size="6"></td>\
        </tr>\
      </table>')
        .append($('<button>').append("Cancel").css('margin-right', '22px').on('click', function() {
          closeWindow(`#${window}`);
        }).css('float', 'right')).append($('<button>').append("Connect").on('click', function() {
          audioPlayer(`${window.split('_')[0]}`, 'play');
          $("input").prop('disabled', true);
          $("button").prop('disabled', true);
          setTimeout(function() {
            if (document.getElementById('Username').value === username && document.getElementById('Password').value === password && document.getElementById('PhoneNumber').value === tel_num && document.getElementById('PhoneNumber2').value === tel_num2) {
              dail_up_connection = true;
              createNewWindow('Connection_Timer', 'body');
              $("input").prop('disabled', false);
              $("button").prop('disabled', false);
              closeWindow(`#${window}`);
            } else {
              $("input").prop('disabled', false);
              $("button").prop('disabled', false);
              createNewWindow('dail-up_error', 'body');
            }
          }, 28000);
        }).css('float', 'right')).append('</p></div>');
    }
  } else if (window === "Connection_Timer") {
    $(`#${window}_content`).append('<div id="timer" style="padding:20px" align="left">\
    <h1>\
        <span id="hour">00</span> :\
        <span id="min">00</span> :\
        <span id="sec">00</span> :\
        <span id="milisec">00</span>\
    </h1>\
    </div>\
    <script>start();</script>').css('z-index', '20001');
  } else if (window === "Minesweeper") {
    $(`#${window}`).css('border-right', '1px solid').css('border-bottom', '1px solid');
    $(`#${window}_content`).append('<iframe id="Minesweeperiframe" />');
    $('#Minesweeperiframe').attr('width', '800px').attr('height', '800px').attr('src', './Games/minesweeper/minesweeper.html');
  } else if (window === "Chess") {
    $(`#${window}_content`).append($(`<div id=chessFrame>`).append('<iframe id="Chessiframe" />').css('padding-top', '10px').css('padding-left', '10px'));
    $('#Chessiframe').attr('width', '400px').attr('height', '400px').attr('src', './Games/chess/index.html');
  } else if (window === "E-Mail") {
    $(`#${window}_content`).append($('<div id="mail_toolbar" class="mail_toolbar">').css('background-color', 'rgb(195, 195, 195)')
        .append($('<button>').append("Get").on('click', function() {
            if (!dail_up_connection) {
              createNewWindow('email_error', 'body');
            } else {
              $(`#mailclient_right_pane`).empty();
              $(`#mailclient_right_pane`).append(dail_up_connection ? `<div class="inbox_detail"><img src="./assets/envelope.png" width="22" height="22""><div id="From" class="mails">info@pcparticulier.nl</div><div id="Subject" class="mails">Configuratie geluidskaart</div><div id="Received" class="mails">${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}</div></div>` : "")
                .on('dblclick', function() {
                  $(`#Configuratie_geluidskaart`).remove();
                  createNewWindow('Configuratie_geluidskaart', 'E-Mail');
                });
            }
          }).css('height', '40px')
          .css('width', '80px')
          .css('font-size', '9px')
          .css('margin', '0px')
          .css('border', '1px solid')
          .css('border-radius', '0px')
          .css('margin', '2px')
          .css('border-radius', '2px'))
        .append($('<button>').append("Compose").on('click', function() {
            createNewWindow('new_e-mail', 'E-Mail');
          }).css('height', '40px')
          .css('width', '80px')
          .css('font-size', '9px')
          .css('margin', '0px')
          .css('border', '1px solid')
          .css('border-radius', '0px')
          .css('margin', '2px')
          .css('border-radius', '2px'))
        .append('</div>'))
      .append($(`<div class="mailWindow">`)
        .append($(`<div></div>`).css('width', 'calc(30% - 4px)').css('margin-right', '8px').css('height', '100%').css('border', '1px solid').css('background-color', 'rgb(255, 255, 255)')
          .append(`<div class="inbox"><div id="private_folders" class="private_folders">Private folders</div></div>`)
          .append($(`<div id="Inbox" class="mail_folders"><img id="inbox_folder" src="./assets/folder_closed.png" width="15" height="14"> Inbox</div>`).on('click', function() {
            $('#inbox_folder').attr('src', './assets/folder_open.png');
            $('#send_mail_folder').attr('src', './assets/folder_closed.png');
            $('#deleted_mail_folder').attr('src', './assets/folder_closed.png');
            $(`#mailclient_right_pane`).empty();
            $(`#mailclient_right_pane`).append(dail_up_connection ? `<div class="inbox_detail"><img src="./assets/envelope.png" width="22" height="22""><div id="From" class="mails">info@pcparticulier.nl</div><div id="Subject" class="mails">Configuratie geluidskaart</div><div id="Received" class="mails">${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}</div></div>` : "")
              .on('dblclick', function() {
                $(`#Configuratie_geluidskaart`).remove();
                createNewWindow('Configuratie_geluidskaart', 'E-Mail');
              });
          }))
          .append($('<div id="Send_mail" class="mail_folders"><img id="send_mail_folder" src="./assets/folder_closed.png" width="15" height="14"> Send mail</div>').on('click', function() {
            $('#inbox_folder').attr('src', './assets/folder_closed.png');
            $('#send_mail_folder').attr('src', './assets/folder_open.png');
            $('#deleted_mail_folder').attr('src', './assets/folder_closed.png');
            $(`#mailclient_right_pane`).empty();
          }))
          .append($('<div id="Deleted_mail" class="mail_folders"><img id="deleted_mail_folder" src="./assets/folder_closed.png" width="15" height="14"> Deleted mail</div>').on('click', function() {
            $('#inbox_folder').attr('src', './assets/folder_closed.png');
            $('#send_mail_folder').attr('src', './assets/folder_closed.png');
            $('#deleted_mail_folder').attr('src', './assets/folder_open.png');
            $(`#mailclient_right_pane`).empty();
          }))
        )
        .append($(`<div></div>`).css('width', 'calc(70% - 4px)').css('height', '100%').css('border', '1px solid').css('background-color', 'rgb(255, 255, 255)')
          .append(`<div class="inbox"><div class="mail_toolbar" width="22" style="border-bottom: 1px solid"></div><div id="From" class="mail_titles">From</div><div id="Subject" class="mail_titles">Subject</div><div id="Received" class="mail_titles">Received</div></div>`)
          .append(`<div id="mailclient_right_pane"></div>`))
      ).css('background-color', 'rgb(195, 195, 195)');
    $(`#mailclient_right_pane`).append(dail_up_connection ? `<div class="inbox_detail"><img src="./assets/envelope.png" width="22" height="22""><div id="From" class="mails">info@pcparticulier.nl</div><div id="Subject" class="mails">Configuratie geluidskaart</div><div id="Received" class="mails">${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}</div></div>` : "")
      .on('dblclick', function() {
        $(`#Configuratie_geluidskaart`).remove();
        createNewWindow('Configuratie_geluidskaart', 'E-Mail');
      });
  } else if (window === "email_error") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="46" height="46"></div>').css('margin', '20px'))
      .append($('<div><h3>No internet connection active.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "Unlock_Screen") {
    let password = "LightsOn";
    let failsavePassword = "lightson";
    let failsavePasswordTwo = "LIGHTSON";
    $(`#${window}`).css("z-index", "20000").css('margin', 'auto').css('top', 'calc(50% - 60px)').css('left', 'calc(50% - 120px)')
    $(`#${window}_content`)
      .append('<div style="padding-top:20px"><input type="password" id="Password" size="30"></div>').on('keypress', function(e) {
        if (e.which == 13) {
          if (document.getElementById('Password').value === password || document.getElementById('Password').value === failsavePassword || document.getElementById('Password').value === failsavePasswordTwo) {
            $('#screensaver').hide();
            $(`#${window}`).remove();
          } else {
            document.getElementById('Password').value = "";
          }
        }
      })
      .append($('<button>').append("OK").on('click', function() {
        if (document.getElementById('Password').value === password || document.getElementById('Password').value === failsavePassword || document.getElementById('Password').value === failsavePasswordTwo) {
          $('#screensaver').hide();
          $(`#${window}`).remove();
        } else {
          document.getElementById('Password').value = "";
        }
      }))
      .append($('<button>').append("Cancel").on('click', function() {
        $(`#${window}`).remove();
      }));

  } else if (window === "1_wav" || window === "2_wav" || window === "3_wav" || window === "4_wav") {
    console.log(`${sound_driver_loaded}, ${interrupt}, ${port}`)
    if (sound_driver_loaded !== "Sound Blaster 16" || interrupt !== "5" || port !== "240") {
      $(`#${window}`).remove();
      createNewWindow('sound_recorder_error', 'body');
    } else {
      $(`#${window}_content`).css('background', 'rgb(195, 195, 195)')
        .append($(`<audio id="${window.split('_')[0]}">`).append(`<source src="./assets/${window.split('_')[0]}.mp3" type="audio/mpeg">`)).append(`</audio>`)
        .append($('<div><span>playing</span></div>'))
        .append(`<img src="./assets/${window}.png" width="290" height="56">`)
        .append($('<button>').css('width', '90px').append(`<img src="./assets/play.png">`).on('click', function() {
          audioPlayer(`${window.split('_')[0]}`, 'play')
        })).append($('<button>pause').css('width', '90px').append(`<img src="./assets/pause.png">`).on('click', function() {
          audioPlayer(`${window.split('_')[0]}`, 'pause')
        })).append($('<button>stop').css('width', '90px').append(`<img src="./assets/stop.png">`).on('click', function() {
          audioPlayer(`${window.split('_')[0]}`, 'stop')
        }));
    }
  } else if (window === "sound_recorder_error") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="58" height="48"></div>').css('margin', '20px'))
      .append($('<div><h3>Sound Player cannot play back because a sound driver is not installed. Use the Driver option in Control Panel to install a driver.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "dail-up_error") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="56" height="56"></div>').css('margin', '20px'))
      .append($('<div><h3>No valid username, password or phone PhoneNumber.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "Drivers") {
    $(`#${window}_content`).append($(`<div class="driversWindow">Installed Drivers</div>`).css('padding', '12px'))
      .append($(`<p>`)
        .append(`<div id="sound_driver_select">Sound Driver<div>`).css('padding', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_select').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#edit_sound_driver').off('click').on('click', function() {
            createNewWindow('Edit', 'body');
          })
        })
        .append(`</p>`).css('float', 'left').css('margin', '12px').css('border', '1px solid').css('width', '70%').css('height', '100px'))
      .append($(`<p>`)
        .append($(`<button>Cancel</button>`).on('click', function() {
          closeWindow(`#${window}`)
        }).css('width', '100px')).append('<div></div>').append($(`<button id="edit_sound_driver">Edit</button>`).on('click', function() {
          createNewWindow('sound_driver_select_error', 'body');
        }).css('width', '100px'))
        .append(`</p>`).css('float', 'right').css('margin', '12px'));
  } else if (window === "Edit") {
    $(`#${window}_content`).append($(`<div class="driversWindow">List of Drivers</div>`).css('padding', '12px'))
      .append($(`<p>`)
        .append($(`<div id="sound_driver_edit_1">MIDI Mapper<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_1').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "MIDI Mapper";
        }))
        .append($(`<div id="sound_driver_edit_2">Roland LABCI<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_2').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "Roland LABCI";
        }))
        .append($(`<div id="sound_driver_edit_3">Sound Blaster 1.0<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_3').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "Sound Blaster 1.0";
        }))
        .append($(`<div id="sound_driver_edit_4">Sound Blaster 16<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_4').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = true;
          sound_driver_loaded = "Sound Blaster 16";
        }))
        .append($(`<div id="sound_driver_edit_5">Sound Blaster 32<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_5').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "Sound Blaster 32";
        }))
        .append($(`<div id="sound_driver_edit_6">Media Vision Thunder Board<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_6').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_7').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "Media Vision Thunder Board";
        }))
        .append($(`<div id="sound_driver_edit_7">[MCI] MIDI Sequencer<div>`).css('padding-left', '4px').css('text-align', 'left').css('cursor', 'default').on('click', function() {
          $('#sound_driver_edit_7').css('background-color', 'rgb(0, 10, 124)').css('color', 'white');
          $('#sound_driver_edit_2').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_3').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_4').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_5').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_6').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          $('#sound_driver_edit_1').css('background-color', 'rgb(255, 255, 255)').css('color', 'black');
          // sound_driver_selected = false;
          sound_driver_loaded = "[MCI] MIDI Sequencer";
        }))
        .append(`</p>`).css('float', 'left').css('margin', '12px').css('border', '1px solid').css('width', '50%').css('height', '100px'))
      .append($(`<p>`)
        .append($(`<button>Cancel</button>`).on('click', function() {
          closeWindow(`#${window}`)
        }).css('width', '100px')).append('<div></div>').append($(`<button id="ok_sound_driver_edit">OK</button>`).on('click', function() {
          // if (sound_driver_selected) {
          createNewWindow('Setup', 'body');
          // } else {
          // createNewWindow('sound_driver_edit_error', 'body');
          // }
        }).css('width', '100px'))
        .append(`</p>`).css('float', 'right').css('margin', '12px'));
  } else if (window === "Setup") {
    $(`#${window}_content`).append($('<div class="driversWindow"></div>').css('padding', '12px'))
      .append($('<p>')
        .append($('<div>Port</div>')
          .append($('<div>')
            .append($('<div><input type="radio" id="210" name="Port" value="210"></div>').append('210'))
            .append($('<div><input type="radio" id="220" name="Port" value="220"></div>').append('220'))
            .append($('<div><input type="radio" id="230" name="Port" value="230"></div>').append('230'))
            .append($('<div><input type="radio" id="240" name="Port" value="240"></div>').append('240'))
            .append($('<div><input type="radio" id="250" name="Port" value="250"></div>').append('250'))
            .append($('<div><input type="radio" id="260" name="Port" value="260"></div>').append('260'))
            .append('</div>').css('height', '130px').css('padding', '10px').css('border', '1px solid'))
          .append($('<button>OK</button>').on('click', function() {
            var setPort = document.getElementsByName('Port');
            var setInterrupt = document.getElementsByName('Interrupt');

            for (var i = 0, length = setPort.length; i < length; i++) {
              if (setPort[i].checked) {
                // do whatever you want with the checked radio
                port = setPort[i].value;
                // only one radio can be logically checked, don't check the rest
                break;
              }
            }
            for (var i = 0, length = setInterrupt.length; i < length; i++) {
              if (setInterrupt[i].checked) {
                // do whatever you want with the checked radio
                interrupt = setInterrupt[i].value;
                // only one radio can be logically checked, don't check the rest
                break;
              }
            }
            closeWindow('#Drivers');
            closeWindow('#Edit');
            closeWindow(`#${window}`);

          }).css('width', '80px').css('margin-top', '12px'))
        )
        .append('</p>').css('float', 'left').css('margin-left', '30px'))
      .append($('<p>')
        .append($('<div>Interrupt</div>')
          .append($('<div>')
            .append($('<div><input type="radio" id="2" name="Interrupt" value="2"></div>').append('2'))
            .append($('<div><input type="radio" id="3" name="Interrupt" value="3"></div>').append('3'))
            .append($('<div><input type="radio" id="5" name="Interrupt" value="5"></div>').append('5'))
            .append($('<div><input type="radio" id="7" name="Interrupt" value="7"></div>').append('7'))
            .append('</div>').css('height', '130px').css('padding', '10px').css('border', '1px solid'))
          .append($('<button>Cancel</button>').on('click', function() {
            closeWindow(`#${window}`)
          }).css('width', '80px').css('margin-top', '12px'))
        )
        .append('</p>').css('float', 'right').css('margin-right', '30px'))
  } else if (window === "sound_driver_select_error") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="56" height="56"></div>').css('margin', '20px'))
      .append($('<div><h3>Please select a device driver type.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "sound_driver_edit_error") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="56" height="56"></div>').css('margin', '20px'))
      .append($('<div><h3>The selected driver is not validated for this computer.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "Pictures") {
    if (!ransomware_code) {
      createNewWindow('Ransomeware', 'body');
      closeWindow(`#${window}`);
    }

  } else if (window === "Ransomeware") {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      // .append($('<div><img src="./assets/error.png" width="58" height="48"></div>').css('margin', '20px'))
      .append($('<div><h3>Oops, your important files are encrypted.</h3><h4>Your pictures have been encrypted. A Private decryption key is soterd on a secred Internet server and nobody can decrypt ypur pictures until you pay and obtain the private key.\
        </br></br>\
        If you don\'t send us money all your pictures will be permanently crypted.</br></br>\
        Pleas fill the decryption key received after payment in below.\
        </h4></div>').css('margin', '20px'))
      .append('</div>')).append($('<div>').append('<input id="one" class="ransom">').append('<input id="two" class="ransom">').append('<input id="three" class="ransom">').append('<input id="four" class="ransom">').append('<input id="five" class="ransom">')).append($('<button>').append("OK").on('click', function() {
      console.log()
      if ($('#one').val() === one && $('#two').val() === two && $('#three').val() === three && $('#four').val() === four && $('#five').val() === five) {
        ransomware_code = true;
        closeWindow(`#${window}`);
        createNewWindow('Pictures', 'body');
      } else {
        createNewWindow('Wrong_Decrypt_key', 'body')
      }

    })).append($('<button>').append("Cancel").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === 'Wrong_Decrypt_key') {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="56" height="56"></div>').css('margin', '20px'))
      .append($('<div><h3>FAIL!!! Wrong decrypt key.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === 'send_email_error') {
    $(`#${window}_content`).append($('<div class="errorMessage">')
      .append($('<div><img src="./assets/error.png" width="56" height="56"></div>').css('margin', '20px'))
      .append($('<div><h3>No email recipient.</h3></div>').css('margin', '20px'))
      .append('</div>')).append($('<button>').append("OK").on('click', function() {
      $(`#${window}`).remove();
    }));
  } else if (window === "new_e-mail") {
    $(`#${window}_content`).append($('<div id="mail_toolbar" class="mail_toolbar">').css('background-color', 'rgb(195, 195, 195)')
      .append($(`<button>Send</button>`).on('click', function() {
        if ($('#to').val().length > 0) {
          closeWindow(`#${window}`);
        } else {
          createNewWindow('send_email_error', 'body');
        }
      }).css('float', 'right'))
      .append($('<div class="mail_detail">')
        .append(`<div><label>To: </label><input id="to"></div>`)
        .append(`<div><label>Subject: </label><input id="subject"></div></div>`)
      ).css('border', '1px solid')
    ).append(`<div class="mail_body"><textarea id="mail_body">`)
  } else if (window === "Configuratie_geluidskaart") {
    $(`#${window}_content`).append($('<div id="mail_toolbar" class="mail_toolbar">').css('background-color', 'rgb(195, 195, 195)')
      .append($('<div class="mail_detail"><div>From: info@pcparticulier.nl<div>')
        .append(`<div>Date: ${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}</div>`)
        .append(`<div>To: me</div>`)
        .append(`<div>Subject: Configuratiegeluidskaart</div></div>`)
      ).css('border', '1px solid')
    ).append(`<div class="mail_body">
        <p>From: info@pcparticulier.nl</p>
        <p>Date: 9-9-2020</p>
        <p>To: bgates@dsu.nl</p>
        <p>Subject: RE: Problemen met geluid</p>
        <p><br></p>
        <p>Beste Bill,</p>
        <p><br></p>
        <p>Dank voor je mail.</p>
        <p>Heb je die 386DX nou nog steeds?</p>
        <p>Het verbaast mij dat hij het nog doet!</p>
        <p><br></p>
        <p>Wat vervelend dat je problemen ondervind.</p>
        <p>Ik moest even graven in mijn geheugen, want een Soundblaster 16 is een tijd geleden dat ik die geinstalleerd heb.</p>
        <p>Maar volgens mij moet je bij &quot;port&quot; kiezen voor 230 of 240 en &quot;interrupt&quot; voor 5 of 6.</p>
        <p>Probeer maar even, foute instellingen maken je computer niet stuk... je hebt alleen geen geluid.</p>
        <p><br></p>
        <p>Ik hoop dat het je zoon trouwens gelukt is om je email instellingen te doen.</p>
        <p>Anders zit ik deze mail voor niks te tikken ;-)</p>
        <p>En mocht het niet lukken. Kom dat gerust even langs dan kijk ik gelijk ook even naar het bestand dat niet meer wil openen.</p>
        <p><br></p>
        <p><br></p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p>$$$$$$$$\ $$\ $$\ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p>$$ &nbsp;_____|$$ |\__| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p>$$ | &nbsp; &nbsp; &nbsp;$$ |$$\ &nbsp;$$$$$$\ &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p>$$$$$\ &nbsp; &nbsp;$$ |$$ |$$ &nbsp;__$$\ &nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p>$$ &nbsp;__| &nbsp; $$ |$$ |$$ / &nbsp;$$ | &nbsp; &nbsp; &nbsp;</p>
        <p>$$ | &nbsp; &nbsp; &nbsp;$$ |$$ |$$ | &nbsp;$$ | &nbsp; &nbsp; &nbsp;</p>
        <p>$$ | &nbsp; &nbsp; &nbsp;$$ |$$ |$$$$$$$ &nbsp;| &nbsp; &nbsp; &nbsp;</p>
        <p>\__| &nbsp; &nbsp; &nbsp;\__|\__|$$ &nbsp;____/ &nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $$ | &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $$ | &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \__| &nbsp; &nbsp;</p>
        <p><br></p>
        <p>------------------------------ Original Mail ------------------------------ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <p><br></p>
        <p>On friday 4 september 16:48 Bill &lt;bgates@dsu.nl&gt; wrote:</p>
        <p><br></p>
        <p>Beste Flip,</p>
        <p><br></p>
        <p>Ik ben mijn computer al flink wat jaren zonder problemen.</p>
        <p>Maar sinds ik een virus heb gehad werkt de boel niet lekker meer.</p>
        <p>Ik heb windows weer opnieuw geinstalleerd maar behoud toch de benodigde problemen.</p>
        <p><br></p>
        <p>De Creative Soundblaster 16 die ik later als update bij jou heb aangeschaft wil geen geluid meer geven.</p>
        <p>Ik vermoed dat de instellingen niet juist zijn. Als ik daar kijken begint te computer over poorten en allerlei andere getallen... en dan weet ik het zelf niet meer hoor.</p>
        <p>Kun jij mij misschien verklappen wat de juiste gegevens zijn?</p>
        <p><br></p>
        <p>Ik verstuur deze mail trouwens vanaf mijn zoon z&#39;n computer want mijn eigen internetverbinding krijg ik ook niet aan de praat.</p>
        <p>Maar stuur gerust je antwoord naar mijn eigen email adres. Rory John zegt dat hij mij wel kan helpen met de email instellingen.</p>
        <p>Als dat gelukt is kijken we samen ook gelijk even naar het geluid en is het handig als we de gegevens alvast hebben.</p>
        <p><br></p>
        <p>Ik las trouwens dat er in onze omgeving veel last is van gijzelsoftware en dat vooral oude systemen de dupe zijn?</p>
        <p>Kan het zijn dat mijn systeem hier misschien last van heeft? De map met foto&#39;s op mijn computer krijg ik niet meer geopend...</p>
        <p>Als ik dat probeer vraagt mijn computer opeens om een wachtwoord?... Dat deed hij voorheen nooit!</p>
        <p>Wellicht doe ik wat fout.</p>
        <p><br></p>
        <p>Mocht ik er met mijn zoon niet uitkomen kom ik wel even langs.</p>
        <p><br></p>
        <p>Alvast bedankt voor de hulp!</p>
        <p><br></p>
        <p><br></p>
        <p>Met vriendelijke groet,</p>
        <p><br></p>
        <p><br></p>
        <p>Bill</p>
        <p><br></p>
        <p><br></p>
      </div>`)
  }

}
//
// Window functions
/////////////////////////////////////////////////////////////

function screensaver() {
  let screensaverTimer = 0;
  if (!$('#Unlock_Screen').length) {
    createNewWindow('Unlock_Screen', 'body');
  }
}

function audioPlayer(song, func) {
  var s = document.getElementById(song);
  console.log(`${song} - ${func}`);

  if (func === "play") {
    s.play();
  }
  if (func === "pause") {
    s.pause();
  }
  if (func === "stop") {
    s.currentTime = 0;
    s.pause();
  }
}

//return an array of objects according to key, value, or key and value matching
function getObjects(obj, key, val) {
  var objects = [];
  for (var i in obj) {
    if (!obj.hasOwnProperty(i)) continue;
    if (typeof obj[i] == 'object') {
      objects = objects.concat(getObjects(obj[i], key, val));
    } else
    //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
    if (i === key && obj[i] === val || i === key && val === '') { //
      objects.push(obj);
    } else if (obj[i] === val && key === '') {
      //only add if the object is not already in the array
      if (objects.lastIndexOf(obj) === -1) {
        objects.push(obj);
      }
    }
  }
  return objects;
}

let toggler = document.getElementsByClassName("caret");
let i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}