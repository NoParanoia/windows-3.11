class Window {
  constructor(id, parent) {
    this.id = id;
    this.parent = parent;
  }
  Main() {
    var win_width;
    var win_height;
    var win_padding;
    var win_border;
    win_width = getObjects(this_json, '', this.id)[0]['width'];
    win_height = getObjects(this_json, '', this.id)[0]['height'];
    if (getObjects(this_json, '', this.id)[0]['resizable']) {
      win_padding = 2;
      win_border = "1px solid";
    } else {
      win_padding = 0;
      win_border = "0px solid";
    }
    if (this.id !== "Program_Manager") {
      if ((topOffset + parseInt(win_height.split('px')[0])) > parseInt(getObjects(this_json, '', this.parent)[0]['height'].split('px')[0])) {
        topOffset = 0;
      }
      if ((leftOffset + parseInt(win_width.split('px')[0])) > parseInt(getObjects(this_json, '', this.parent)[0]['width'].split('px')[0])) {
        leftOffset = 0;
      }
      topOffset = topOffset + 23;
      leftOffset = leftOffset + 4;
    }
    return `<div id="${this.id}" class="window" style="width: ${win_width}; height: ${win_height}; padding: ${win_padding}px; border:${win_border}; top:${this.id === "Program_Manager" ? "0" : topOffset}px; left:${this.id === "Program_Manager" ? "0" : leftOffset}px;">`;
  }
  TitleBar() {
    var border_left = "";
    var border_right = "";
    if (getObjects(this_json, '', this.id)[0]['menu_button']) {
      border_left = "border-left: 0px solid; border-color: black;";
    } else {
      border_left = "border-left: 1px solid; border-color: black;";
    }
    if (!getObjects(this_json, '', this.id)[0]['min_button'] && !getObjects(this_json, '', this.id)[0]['max_button']) {
      border_right = "border-right: 1px solid; border-color: black;";
    } else {
      border_right = "border-right: 0px solid; border-color: black;";
    }
    return `<div class=titleBar id="${this.id}_titleBar" style="${border_left}; ${border_right}"><div>`;
  }
  Title() {
    var buttons = 0;
    if (getObjects(this_json, '', this.id)[0]['menu_button']) {
      buttons += 1;
    }
    if (getObjects(this_json, '', this.id)[0]['min_button']) {
      buttons += 1;
    }
    if (getObjects(this_json, '', this.id)[0]['max_button']) {
      buttons += 1;
    }
    return `<div class="windowTitle" id="${this.id}_windowTitle" style="width: calc(100% - ${18*buttons}px);">${this.id.replace(/_/g, " ").replace(/\+/g, " ")}</div>`;
  }
  MenuButton() {
    return getObjects(this_json, '', this.id)[0]['menu_button'] ?
      this.id == "Program_Manager" ?
      `<div class="buttonMenu" id="${this.id}_buttonMenu"></div>` :
      `<div class="buttonMenuSub" id="${this.id}_buttonMenu"></div>` :
      "";
  }
  MinButton() {
    return getObjects(this_json, '', this.id)[0]['min_button'] ?
      `<a href="#" class="buttonMin" id="${this.id}_buttonMin" onClick="minimizeWindow('#${this.id}')"></a>` :
      "";
  }
  MaxButton() {
    return getObjects(this_json, '', this.id)[0]['max_button'] ?
      `<a href="#" class="buttonMax" id="${this.id}_buttonFull" onClick="maximizeWindow('#${this.id}')"></a>` :
      "";
  }
  ResizeButton() {
    return getObjects(this_json, '', this.id)[0]['resizable'] ?
      `<a href="#" class="buttonResize" id="${this.id}_buttonResize" onClick="resizeWindow('#${this.id}')"></a>` :
      "";
  }
  Content() {
    return `<div class="windowContent" id="${this.id}_content">`;
  }
  IconGrid() {
    return this.id == "Program_Manager" ?
      `<div class="iconGridProgramManager" id="${this.id}_iconGridWindow"></div>` :
      `<div class="iconGridWindow" id="${this.id}_iconGridWindow"></div>`;
  }
  Icons() {
    var this_name;
    var this_width;
    var this_height;
    var this_resize;
    var this_icon;
    var return_value = "";
    if (!getObjects(this_json, '', this.id.replace('_icon', ''))[0]['application']) {
      $(`#${this.id}_icon`).hide();
    }
    var object = getObjects(this_json, '', this.id)[0]['sub_windows'];
    for (var value in object) {
      this_name = object[value]['name'];
      this_icon = object[value]['icon'];
      return_value = return_value + `<div class=icon id="${this_name}_icon" style="left: ${(96*value)}px;"><img src="${this_icon}" width="46" height="46"><p>${this_name.replace(/_/g, " ")}</p></div>`;
    }
    return return_value;
  }

  ConfigureIcons() {
    $(`#${this.id}_iconGridWindow`).children().draggable({
      zIndex: 1,
      containment: `#${this.id}_content`
    }).on('dblclick', function() {
      if ($(`#${this.id.replace('_icon', '')}`).length > 0) {
        if (!getObjects(this_json, '', this.id.replace('_icon', ''))[0]['application']) {
          $(`#${this.id}`).hide();
        }
        $(`#${this.id.replace('_icon', '')}`).show();
      } else {
        if (!getObjects(this_json, '', this.id.replace('_icon', ''))[0]['application']) {
          createNewWindow(`${this.id.replace('_icon', '')}`, 'Program_Manager');
        } else {
          createNewWindow(`${this.id.replace('_icon', '')}`, 'body');
        }
      }
    }).on('click', function() {
      setIconActive(`${this.id}`)
    })
  }

  Draggable() {
    return {
      handle: `#${this.id}_windowTitle`,
      containment: this.parent === 'body' ? 'body' : `#${this.parent}_content`
    }
  }

  Resizable() {
    return {
      minHeigh: 100,
      minWidth: 100,
      handles: "n, e, s, w, ne, se, sw, nw",
      classes: {
        "ui-dialog": "ui-corner-all custom-red"
      },
      zIndex: 1
    };
  }
}